﻿namespace BrowseEmAll.Cef.Client
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using BrowseEmAll.Cef;

    internal sealed class DemoApp : CefApp
    {
        private int remoteDebugPort;

        public DemoApp(int remoteDebugPort)
        {
            this.remoteDebugPort = remoteDebugPort;
        }

        protected override void OnBeforeCommandLineProcessing(string processType, CefCommandLine commandLine)
        {
            commandLine.AppendSwitch("remote-debugging-port", remoteDebugPort.ToString());
        }
    }
}
