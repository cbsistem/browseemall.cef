﻿namespace BrowseEmAll.Cef.Client
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Windows.Forms;
    using BrowseEmAll.Cef;

    internal static class Program
    {
        [STAThread]
        private static int Main(string[] args)
        {
            try
            {
                string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + (Environment.Is64BitProcess ? @"\CEF64\" : @"\CEF32\");
                CefRuntime.Load(path);
            }
            catch (DllNotFoundException ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 1;
            }
            catch (CefRuntimeException ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 2;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 3;
            }

            var mainArgs = new CefMainArgs(args);
            var app = new DemoApp(36888);

            var exitCode = CefRuntime.ExecuteProcess(mainArgs, app, IntPtr.Zero);
            if (exitCode != -1)
                return exitCode;

            var settings = new CefSettings
                {             
                    SingleProcess = false,
                    MultiThreadedMessageLoop = true,
                    LogSeverity = CefLogSeverity.Verbose
                };

            CefRuntime.Initialize(mainArgs, settings, app, IntPtr.Zero);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Application.Run(new MainForm());

            CefRuntime.Shutdown();
            return 0;
        }
    }
}
