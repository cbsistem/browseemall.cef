﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrowseEmAll.Cef.Winforms
{
    internal sealed class CefWebKeyboardHandler : CefKeyboardHandler
    {
        private readonly CefWebBrowser _core;

        public CefWebKeyboardHandler(CefWebBrowser core)
        {
            _core = core;
        }

        protected override bool OnKeyEvent(CefBrowser browser, CefKeyEvent keyEvent, IntPtr osEvent)
        {
            return _core.OnKeyEvent(keyEvent);
        }
    }
}
