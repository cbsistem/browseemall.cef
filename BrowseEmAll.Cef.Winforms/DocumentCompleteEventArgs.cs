﻿using System;

namespace BrowseEmAll.Cef.Winforms
{
	public class DocumentCompleteEventArgs : EventArgs
	{
		public DocumentCompleteEventArgs(CefFrame frame, int httpStatusCode)
		{
			Frame = frame;
			HttpStatusCode = httpStatusCode;
		}

		public int HttpStatusCode { get; private set; }

		public CefFrame Frame { get; private set; }
	}
}