﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrowseEmAll.Cef.Winforms
{
    public class ActionPerformedEventArgs : EventArgs
    {
        // -1 nothing, 0 bool, 1 double, 2 string
        public string Data { get; set; }
    }
}
