﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrowseEmAll.Cef.Winforms
{
    public class CustomKeyEventArgs:EventArgs
    {
        public bool IsStrg { get; set; }
        public int KeyCode { get; set; }
    }
}
