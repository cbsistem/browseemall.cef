﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrowseEmAll.Cef.Winforms
{
    public class ContextMenuEventArgs : EventArgs
    {
        public string Url { get; set; }
    }
}
