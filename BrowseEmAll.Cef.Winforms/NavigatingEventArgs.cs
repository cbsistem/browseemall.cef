﻿using System;

namespace BrowseEmAll.Cef.Winforms
{
	public class NavigatingEventArgs : EventArgs
	{
		public NavigatingEventArgs(CefFrame frame)
		{
			Frame = frame;
		}

		public CefFrame Frame { get; private set; }
	}
}