﻿using System;

namespace BrowseEmAll.Cef.Winforms
{
	public class RenderProcessTerminatedEventArgs : EventArgs
	{
		public RenderProcessTerminatedEventArgs(CefTerminationStatus status)
		{
			Status = status;
		}

		public CefTerminationStatus Status { get; private set; }
	}
}