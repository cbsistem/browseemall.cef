﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrowseEmAll.Cef.Winforms
{
    public class ScreenshotEventArgs : EventArgs
    {
        public byte[] Screenshot { get; set; }
    }
}
