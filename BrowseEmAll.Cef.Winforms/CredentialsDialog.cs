﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Security.Permissions;
using BrowseEmAll.Cef.Winforms;

namespace DataCollectionShared
{
    /// <summary>Encapsulates dialog functionality from the Credential Management API.</summary>
    public sealed class CredentialsDialog2
    {
        /// <summary>The only valid bitmap height (in pixels) of a user-defined banner.</summary>
        private const int ValidBannerHeight = 60;
        /// <summary>The only valid bitmap width (in pixels) of a user-defined banner.</summary>
        private const int ValidBannerWidth = 320;

        /// <summary>Initializes a new instance of the <see cref="T:SecureCredentialsLibrary.CredentialsDialog"/> class
        /// with the specified target.</summary>
        /// <param name="target">The name of the target for the credentials, typically a server name.</param>
        public CredentialsDialog2(string target)
            : this(target, null)
        { }
        /// <summary>Initializes a new instance of the <see cref="T:SecureCredentialsLibrary.CredentialsDialog"/> class
        /// with the specified target and caption.</summary>
        /// <param name="target">The name of the target for the credentials, typically a server name.</param>
        /// <param name="caption">The caption of the dialog (null will cause a system default title to be used).</param>
        public CredentialsDialog2(string target, string caption)
            : this(target, caption, null)
        { }
        /// <summary>Initializes a new instance of the <see cref="T:SecureCredentialsLibrary.CredentialsDialog"/> class
        /// with the specified target, caption and message.</summary>
        /// <param name="target">The name of the target for the credentials, typically a server name.</param>
        /// <param name="caption">The caption of the dialog (null will cause a system default title to be used).</param>
        /// <param name="message">The message of the dialog (null will cause a system default message to be used).</param>
        public CredentialsDialog2(string target, string caption, string message)
            : this(target, caption, message, null)
        { }
        /// <summary>Initializes a new instance of the <see cref="T:SecureCredentialsLibrary.CredentialsDialog"/> class
        /// with the specified target, caption, message and banner.</summary>
        /// <param name="target">The name of the target for the credentials, typically a server name.</param>
        /// <param name="caption">The caption of the dialog (null will cause a system default title to be used).</param>
        /// <param name="message">The message of the dialog (null will cause a system default message to be used).</param>
        /// <param name="banner">The image to display on the dialog (null will cause a system default image to be used).</param>
        public CredentialsDialog2(string target, string caption, string message, Image banner)
        {
            this.Target = target;
            this.Caption = caption;
            this.Message = message;
            this.Banner = banner;
        }

        private bool _alwaysDisplay = false;
        /// <summary>
        /// Gets or sets if the dialog will be shown even if the credentials
        /// can be returned from an existing credential in the credential manager.
        /// </summary>
        public bool AlwaysDisplay
        {
            get
            {
                return _alwaysDisplay;
            }
            set
            {
                _alwaysDisplay = value;
            }
        }

        private bool _excludeCertificates = true;
        /// <summary>Gets or sets if the dialog is populated with name/password only.</summary>
        public bool ExcludeCertificates
        {
            get
            {
                return _excludeCertificates;
            }
            set
            {
                _excludeCertificates = value;
            }
        }

        private bool _persist = true;
        /// <summary>Gets or sets if the credentials are to be persisted in the credential manager.</summary>
        public bool Persist
        {
            get
            {
                return _persist;
            }
            set
            {
                _persist = value;
            }
        }

        private bool _keepName = false;
        /// <summary>Gets or sets if the name is read-only.</summary>
        public bool KeepName
        {
            get
            {
                return _keepName;
            }
            set
            {
                _keepName = value;
            }
        }

        private string _name = String.Empty;
        /// <summary>Gets or sets the name for the credentials.</summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2208:InstantiateArgumentExceptionsCorrectly")]
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value != null)
                {
                    if (value.Length > NativeMethods.MAX_USERNAME_LENGTH)
                    {
                        string message = String.Format(
                            Thread.CurrentThread.CurrentUICulture,
                            "The name has a maximum length of {0} characters.",
                            NativeMethods.MAX_USERNAME_LENGTH);
                        throw new ArgumentException(message, "Name");
                    }
                }
                _name = value;
            }
        }

        private string _password = String.Empty;
        /// <summary>Gets or sets the password for the credentials.</summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2208:InstantiateArgumentExceptionsCorrectly")]
        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                if (value != null)
                {
                    if (value.Length > NativeMethods.MAX_PASSWORD_LENGTH)
                    {
                        string message = String.Format(
                            Thread.CurrentThread.CurrentUICulture,
                            "The password has a maximum length of {0} characters.",
                            NativeMethods.MAX_PASSWORD_LENGTH);
                        throw new ArgumentException(message, "Password");
                    }
                }
                _password = value;
            }
        }

        private bool _saveChecked = false;
        /// <summary>Gets or sets if the save checkbox status.</summary>
        public bool SaveChecked
        {
            get
            {
                return _saveChecked;
            }
            set
            {
                _saveChecked = value;
            }
        }

        private bool _saveDisplayed = true;
        /// <summary>Gets or sets if the save checkbox is displayed.</summary>
        /// <remarks>This value only has effect if Persist is true.</remarks>
        public bool SaveDisplayed
        {
            get
            {
                return _saveDisplayed;
            }
            set
            {
                _saveDisplayed = value;
            }
        }

        private string _target = String.Empty;
        /// <summary>Gets or sets the name of the target for the credentials, typically a server name.</summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2208:InstantiateArgumentExceptionsCorrectly")]
        public string Target
        {
            get
            {
                return _target;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentException("The target cannot be a null value.", "Target");
                }
                else if (value.Length > NativeMethods.MAX_GENERIC_TARGET_LENGTH)
                {
                    string message = String.Format(
                        Thread.CurrentThread.CurrentUICulture,
                        "The target has a maximum length of {0} characters.",
                        NativeMethods.MAX_GENERIC_TARGET_LENGTH);
                    throw new ArgumentException(message, "Target");
                }
                _target = value;
            }
        }

        private string _caption = String.Empty;
        /// <summary>Gets or sets the caption of the dialog.</summary>
        /// <remarks>A null value will cause a system default caption to be used.</remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2208:InstantiateArgumentExceptionsCorrectly")]
        public string Caption
        {
            get
            {
                return _caption;
            }
            set
            {
                if (value != null)
                {
                    if (value.Length > NativeMethods.MAX_CAPTION_LENGTH)
                    {
                        string message = String.Format(
                            Thread.CurrentThread.CurrentUICulture,
                            "The caption has a maximum length of {0} characters.",
                            NativeMethods.MAX_CAPTION_LENGTH);
                        throw new ArgumentException(message, "Caption");
                    }
                }
                _caption = value;
            }
        }

        private string _message = String.Empty;
        /// <summary>Gets or sets the message of the dialog.</summary>
        /// <remarks>A null value will cause a system default message to be used.</remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2208:InstantiateArgumentExceptionsCorrectly")]
        public string Message
        {
            get
            {
                return _message;
            }
            set
            {
                if (value != null)
                {
                    if (value.Length > NativeMethods.MAX_MESSAGE_LENGTH)
                    {
                        string message = String.Format(
                            Thread.CurrentThread.CurrentUICulture,
                            "The message has a maximum length of {0} characters.",
                            NativeMethods.MAX_MESSAGE_LENGTH);
                        throw new ArgumentException(message, "Message");
                    }
                }
                _message = value;
            }
        }

        private Image _banner = null;
        /// <summary>Gets or sets the image to display on the dialog.</summary>
        /// <remarks>A null value will cause a system default image to be used.</remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2208:InstantiateArgumentExceptionsCorrectly")]
        public Image Banner
        {
            get
            {
                return _banner;
            }
            set
            {
                if (value != null)
                {
                    if (value.Width != ValidBannerWidth)
                    {
                        throw new ArgumentException("The banner image width must be 320 pixels.", "Banner");
                    }
                    if (value.Height != ValidBannerHeight)
                    {
                        throw new ArgumentException("The banner image height must be 60 pixels.", "Banner");
                    }
                }
                _banner = value;
            }
        }

        /// <summary>Shows the credentials dialog.</summary>
        /// <returns>Returns a DialogResult indicating the user action.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2135:SecurityRuleSetLevel2MethodsShouldNotBeProtectedWithLinkDemandsFxCopRule"), EnvironmentPermissionAttribute(SecurityAction.LinkDemand, Unrestricted = true)]
        public DialogResult Show()
        {
            return Show(null, this.Name, this.Password, this.SaveChecked);
        }

        /// <summary>Shows the credentials dialog with the specified save checkbox status.</summary>
        /// <param name="saveChecked">True if the save checkbox is checked.</param>
        /// <returns>Returns a DialogResult indicating the user action.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2135:SecurityRuleSetLevel2MethodsShouldNotBeProtectedWithLinkDemandsFxCopRule"), EnvironmentPermissionAttribute(SecurityAction.LinkDemand, Unrestricted = true)]
        public DialogResult Show(bool saveChecked)
        {
            return Show(null, this.Name, this.Password, saveChecked);
        }

        /// <summary>Shows the credentials dialog with the specified name.</summary>
        /// <param name="name">The name for the credentials.</param>
        /// <returns>Returns a DialogResult indicating the user action.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2135:SecurityRuleSetLevel2MethodsShouldNotBeProtectedWithLinkDemandsFxCopRule"), EnvironmentPermissionAttribute(SecurityAction.LinkDemand, Unrestricted = true)]
        public DialogResult Show(string name)
        {
            return Show(null, name, this.Password, this.SaveChecked);
        }

        /// <summary>Shows the credentials dialog with the specified name and password.</summary>
        /// <param name="name">The name for the credentials.</param>
        /// <param name="password">The password for the credentials.</param>
        /// <returns>Returns a DialogResult indicating the user action.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2135:SecurityRuleSetLevel2MethodsShouldNotBeProtectedWithLinkDemandsFxCopRule"), EnvironmentPermissionAttribute(SecurityAction.LinkDemand, Unrestricted = true)]
        public DialogResult Show(string name, string password)
        {
            return Show(null, name, password, this.SaveChecked);
        }

        /// <summary>Shows the credentials dialog with the specified name, password and save checkbox status.</summary>
        /// <param name="name">The name for the credentials.</param>
        /// <param name="password">The password for the credentials.</param>
        /// <param name="saveChecked">True if the save checkbox is checked.</param>
        /// <returns>Returns a DialogResult indicating the user action.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2135:SecurityRuleSetLevel2MethodsShouldNotBeProtectedWithLinkDemandsFxCopRule"), EnvironmentPermissionAttribute(SecurityAction.LinkDemand, Unrestricted = true)]
        public DialogResult Show(string name, string password, bool saveChecked)
        {
            return Show(null, name, password, saveChecked);
        }

        /// <summary>Shows the credentials dialog with the specified owner.</summary>
        /// <param name="owner">The System.Windows.Forms.IWin32Window the dialog will display in front of.</param>
        /// <returns>Returns a DialogResult indicating the user action.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2135:SecurityRuleSetLevel2MethodsShouldNotBeProtectedWithLinkDemandsFxCopRule"), EnvironmentPermissionAttribute(SecurityAction.LinkDemand, Unrestricted = true)]
        public DialogResult Show(IWin32Window owner)
        {
            return Show(owner, this.Name, this.Password, this.SaveChecked);
        }

        /// <summary>Shows the credentials dialog with the specified owner and save checkbox status.</summary>
        /// <param name="owner">The System.Windows.Forms.IWin32Window the dialog will display in front of.</param>
        /// <param name="saveChecked">True if the save checkbox is checked.</param>
        /// <returns>Returns a DialogResult indicating the user action.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2135:SecurityRuleSetLevel2MethodsShouldNotBeProtectedWithLinkDemandsFxCopRule"), EnvironmentPermissionAttribute(SecurityAction.LinkDemand, Unrestricted = true)]
        public DialogResult Show(IWin32Window owner, bool saveChecked)
        {
            return Show(owner, this.Name, this.Password, saveChecked);
        }

        /// <summary>Shows the credentials dialog with the specified owner, name and password.</summary>
        /// <param name="owner">The System.Windows.Forms.IWin32Window the dialog will display in front of.</param>
        /// <param name="name">The name for the credentials.</param>
        /// <param name="password">The password for the credentials.</param>
        /// <returns>Returns a DialogResult indicating the user action.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2135:SecurityRuleSetLevel2MethodsShouldNotBeProtectedWithLinkDemandsFxCopRule"), EnvironmentPermissionAttribute(SecurityAction.LinkDemand, Unrestricted = true)]
        public DialogResult Show(IWin32Window owner, string name, string password)
        {
            return Show(owner, name, password, this.SaveChecked);
        }

        /// <summary>Shows the credentials dialog with the specified owner, name, password and save checkbox status.</summary>
        /// <param name="owner">The System.Windows.Forms.IWin32Window the dialog will display in front of.</param>
        /// <param name="name">The name for the credentials.</param>
        /// <param name="password">The password for the credentials.</param>
        /// <param name="saveChecked">True if the save checkbox is checked.</param>
        /// <returns>Returns a DialogResult indicating the user action.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2135:SecurityRuleSetLevel2MethodsShouldNotBeProtectedWithLinkDemandsFxCopRule"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2201:DoNotRaiseReservedExceptionTypes"), EnvironmentPermissionAttribute(SecurityAction.LinkDemand, Unrestricted = true)]
        public DialogResult Show(IWin32Window owner, string name, string password, bool saveChecked)
        {
            if (Environment.OSVersion.Version.Major < 5)
            {
                throw new ApplicationException("The Credential Management API requires Windows XP / Windows Server 2003 or later.");
            }
            this.Name = name;
            this.Password = password;
            this.SaveChecked = saveChecked;

            return ShowDialog(owner);
        }

        /// <summary>Confirmation action to be applied.</summary>
        /// <param name="value">True if the credentials should be persisted.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2201:DoNotRaiseReservedExceptionTypes")]
        public void Confirm(bool value)
        {
            switch (NativeMethods.ConfirmCredentials(this.Target, value))
            {
                case NativeMethods.ReturnCodes.NO_ERROR:
                    break;

                case NativeMethods.ReturnCodes.ERROR_INVALID_PARAMETER:
                    // for some reason, this is encountered when credentials are overwritten
                    break;

                default:
                    throw new ApplicationException("Credential confirmation failed.");
            }
        }

        /// <summary>Returns a DialogResult indicating the user action.</summary>
        /// <param name="owner">The System.Windows.Forms.IWin32Window the dialog will display in front of.</param>
        /// <remarks>
        /// Sets the name, password and SaveChecked accessors to the state of the dialog as it was dismissed by the user.
        /// </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2135:SecurityRuleSetLevel2MethodsShouldNotBeProtectedWithLinkDemandsFxCopRule"), EnvironmentPermissionAttribute(SecurityAction.LinkDemand, Unrestricted = true)]
        private DialogResult ShowDialog(IWin32Window owner)
        {
            // set the api call parameters
            StringBuilder name = new StringBuilder(NativeMethods.MAX_USERNAME_LENGTH);
            name.Append(this.Name);

            StringBuilder password = new StringBuilder(NativeMethods.MAX_PASSWORD_LENGTH);
            password.Append(this.Password);

            int saveChecked = Convert.ToInt32(this.SaveChecked);

            NativeMethods.INFO info = GetInfo(owner);
            NativeMethods.FLAGS flags = GetFlags();

            // make the api call
            NativeMethods.ReturnCodes code = NativeMethods.PromptForCredentials(
                ref info,
                this.Target,
                IntPtr.Zero, 0,
                name, NativeMethods.MAX_USERNAME_LENGTH,
                password, NativeMethods.MAX_PASSWORD_LENGTH,
                ref saveChecked,
                flags
                );

            // clean up resources
            if (this.Banner != null) NativeMethods.DeleteObject(info.hbmBanner);

            // set the accessors from the api call parameters
            this.Name = name.ToString();
            this.Password = password.ToString();
            this.SaveChecked = Convert.ToBoolean(saveChecked);

            return GetDialogResult(code);
        }

        /// <summary>Returns the info structure for dialog display settings.</summary>
        /// <param name="owner">The System.Windows.Forms.IWin32Window the dialog will display in front of.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2135:SecurityRuleSetLevel2MethodsShouldNotBeProtectedWithLinkDemandsFxCopRule"), EnvironmentPermissionAttribute(SecurityAction.LinkDemand, Unrestricted = true)]
        private NativeMethods.INFO GetInfo(IWin32Window owner)
        {
            NativeMethods.INFO info = new NativeMethods.INFO();
            if (owner != null) info.hwndParent = owner.Handle;
            info.pszCaptionText = this.Caption;
            info.pszMessageText = this.Message;
            if (this.Banner != null)
            {
                using (Bitmap Bitmap = new Bitmap(this.Banner, ValidBannerWidth, ValidBannerHeight))
                {
                    info.hbmBanner = Bitmap.GetHbitmap();
                }
            }
            info.cbSize = Marshal.SizeOf(info);
            return info;
        }

        /// <summary>Returns the flags for dialog display options.</summary>
        private NativeMethods.FLAGS GetFlags()
        {
            NativeMethods.FLAGS flags = NativeMethods.FLAGS.GENERIC_CREDENTIALS;

            // grrrr... can't seem to get this to work...
            // if (incorrectPassword) flags = flags | CredUI.CREDUI_FLAGS.INCORRECT_PASSWORD;

            if (this.AlwaysDisplay) flags = flags | NativeMethods.FLAGS.ALWAYS_SHOW_UI;

            if (this.ExcludeCertificates) flags = flags | NativeMethods.FLAGS.EXCLUDE_CERTIFICATES;

            if (this.Persist)
            {
                flags = flags | NativeMethods.FLAGS.EXPECT_CONFIRMATION;
                if (this.SaveDisplayed) flags = flags | NativeMethods.FLAGS.SHOW_SAVE_CHECK_BOX;
            }
            else
            {
                flags = flags | NativeMethods.FLAGS.DO_NOT_PERSIST;
            }

            if (this.KeepName) flags = flags | NativeMethods.FLAGS.KEEP_USERNAME;

            return flags;
        }

        /// <summary>Returns a DialogResult from the specified code.</summary>
        /// <param name="code">The credential return code.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2201:DoNotRaiseReservedExceptionTypes")]
        private DialogResult GetDialogResult(NativeMethods.ReturnCodes code)
        {
            DialogResult result;
            switch (code)
            {
                case NativeMethods.ReturnCodes.NO_ERROR:
                    result = DialogResult.OK;
                    break;

                case NativeMethods.ReturnCodes.ERROR_CANCELLED:
                    result = DialogResult.Cancel;
                    break;

                case NativeMethods.ReturnCodes.ERROR_NO_SUCH_LOGON_SESSION:
                    throw new ApplicationException("No such logon session.");

                case NativeMethods.ReturnCodes.ERROR_NOT_FOUND:
                    throw new ApplicationException("Not found.");

                case NativeMethods.ReturnCodes.ERROR_INVALID_ACCOUNT_NAME:
                    throw new ApplicationException("Invalid account name.");

                case NativeMethods.ReturnCodes.ERROR_INSUFFICIENT_BUFFER:
                    throw new ApplicationException("Insufficient buffer.");

                case NativeMethods.ReturnCodes.ERROR_INVALID_PARAMETER:
                    throw new ApplicationException("Invalid parameter.");

                case NativeMethods.ReturnCodes.ERROR_INVALID_FLAGS:
                    throw new ApplicationException("Invalid flags.");

                default:
                    throw new ApplicationException("Unknown credential result encountered.");
            }
            return result;
        }
    }
}