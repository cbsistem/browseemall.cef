﻿using System;
using System.Collections.Specialized;
namespace BrowseEmAll.Cef.Winforms
{
    sealed class CefWebRequestHandler : CefRequestHandler
    {
        private readonly CefWebBrowser _core;

        public CefWebRequestHandler(CefWebBrowser core)
        {
            _core = core;
        }

        protected override void OnPluginCrashed(CefBrowser browser, string pluginPath)
        {
            _core.InvokeIfRequired(() => _core.OnPluginCrashed(new PluginCrashedEventArgs(pluginPath)));
        }

        protected override void OnRenderProcessTerminated(CefBrowser browser, CefTerminationStatus status)
        {
            _core.InvokeIfRequired(() => _core.OnRenderProcessTerminated(new RenderProcessTerminatedEventArgs(status)));
        }

        protected override bool GetAuthCredentials(CefBrowser browser, CefFrame frame, bool isProxy, string host, int port, string realm, string scheme, CefAuthCallback callback)
        {
            string username = string.Empty;
            string password = string.Empty;
            bool result = _core.GetAuthCredentials(out username, out password);

            if (result)
                callback.Continue(username, password);
            else
                callback.Cancel();

            return result;
        }

        protected override CefReturnValue OnBeforeResourceLoad(CefBrowser browser, CefFrame frame, CefRequest request, CefRequestCallback callback)
        {
            if (!string.IsNullOrEmpty(_core.UserAgent))
            {
                NameValueCollection headers = request.GetHeaderMap();
                headers["User-Agent"] = _core.UserAgent;
                request.SetHeaderMap(headers);
            }
            return CefReturnValue.Continue;
        }

        protected override bool OnBeforePluginLoad(CefBrowser browser, string url, string policyUrl, CefWebPluginInfo info)
        {
            bool result = false;
            _core.InvokeIfRequired(() => result = _core.OnBeforePluginLoad(info));

            return result;
        }

        protected override bool OnCertificateError(CefBrowser browser, CefErrorCode certError, string requestUrl, CefSSLInfo sslInfo, CefRequestCallback callback)
        {            
            callback.Continue(true);
            return true;
        }

    }
}