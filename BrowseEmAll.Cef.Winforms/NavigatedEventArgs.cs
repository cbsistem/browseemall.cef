﻿using System;

namespace BrowseEmAll.Cef.Winforms
{
	public class NavigatedEventArgs : EventArgs
	{
		public NavigatedEventArgs(CefFrame frame, string address)
		{
			Address = address;
			Frame = frame;
		}

		public string Address { get; private set; }

		public CefFrame Frame { get; private set; }
	}
}
