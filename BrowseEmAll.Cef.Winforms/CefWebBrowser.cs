﻿namespace BrowseEmAll.Cef.Winforms
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Windows.Forms;
    using DataCollectionShared;
    using System.Threading.Tasks;
    using System.IO;
    using System.Drawing.Imaging;
    using Native;
    [ToolboxBitmap(typeof(CefWebBrowser))]
    public class CefWebBrowser : Control
    {
        private bool _handleCreated;

        private CefBrowser _browser;
        private IntPtr _browserWindowHandle;

        private CefWebClient _client;

        private string BrowserIdentifier;

        private const int MAX_SCREENSHOT_RENDERS = 5;

        public bool WillBePopup { get; set; }

        private static readonly Keys[] HandledKeys =
        {
            Keys.Tab, Keys.Home, Keys.End, Keys.Left, Keys.Right, Keys.Up, Keys.Down
        };
        private Timer screenshotTimer;
        private int currentScreenshotRender = 0;

        private bool ignoreSSL;

        public CefWebBrowser(string browserIdentifier, bool ignoreSSL = false)
        {
            SetStyle(
                ControlStyles.ContainerControl
                | ControlStyles.ResizeRedraw
                | ControlStyles.FixedWidth
                | ControlStyles.FixedHeight
                | ControlStyles.StandardClick
                | ControlStyles.UserMouse
                | ControlStyles.SupportsTransparentBackColor
                | ControlStyles.StandardDoubleClick
                | ControlStyles.OptimizedDoubleBuffer
                | ControlStyles.CacheText
                | ControlStyles.EnableNotifyMessage
                | ControlStyles.DoubleBuffer
                | ControlStyles.OptimizedDoubleBuffer
                | ControlStyles.UseTextForAccessibility
                | ControlStyles.Opaque,
                false);

            SetStyle(
                ControlStyles.UserPaint
                | ControlStyles.AllPaintingInWmPaint
                | ControlStyles.Selectable,
                true);

            this.ignoreSSL = ignoreSSL;

            StartUrl = "about:blank";
            Silent = false;
            DisablePlugins = false;
            IsOffScreen = false;
            GettingScreenshot = false;
            BrowserIdentifier = browserIdentifier;
        }

        /// <summary>
        /// Gets a value indicating whether a previous page in navigation history is available
        /// </summary>
        public bool CanGoBack { get; private set; }

        /// <summary>
        /// Gets a value indicating whether a subsequent page in navigation history is available
        /// </summary>
        public bool CanGoForward { get; private set; }

        /// <summary>
        /// Gets the title of the document currently displayed
        /// </summary>
        public string DocumentTitle { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the control is currently loading a document
        /// </summary>
        public bool IsBusy { get; private set; }

        /// <summary>
        /// Gets the status text of the control
        /// </summary>
        public string StatusText { get; private set; }

        /// <summary>
        /// Gets the URL of the current document.
        /// </summary>
        public Uri Url { get; private set; }

        /// <summary>
        /// Gets or sets the browser zoom level. The default zoom level is 0.0
        /// </summary>
        public double ZoomLevel
        {
            get
            {
                return _browser.GetHost().GetZoomLevel();
            }
            set
            {
                _browser.GetHost().SetZoomLevel(value);
            }
        }

        public bool IsBrowserCreated
        {
            get
            {
                return _browser != null;
            }
        }

        [DefaultValue("about:blank")]
        public string StartUrl { get; set; }

        public bool IsOffScreen { get; set; }

        public int OffScreen_Width { get; set; }

        public int OffScreen_Height { get; set; }

        public bool GettingScreenshot { get; set; }

        public byte[] LastScreenshot { get; set; }

        [Browsable(false)]
        public CefBrowserSettings BrowserSettings { get; set; }              

        public bool Silent { get; set; }

        public bool DisablePlugins { get; set; }

        public string UserAgent { get; set; }

        // Public Methods
        /// <summary>
        /// Navigates the control to the previous page in the history
        /// </summary>
        /// <returns></returns>
        public bool GoBack()
        {
            if (_browser == null)
                return false;

            _browser.GoBack();
            return true;
        }

        /// <summary>
        /// Navigates the control to the next page in the history
        /// </summary>
        /// <returns></returns>
        public bool GoForward()
        {
            if (_browser == null)
                return false;

            _browser.GoForward();
            return true;
        }

        /// <summary>
        /// Loads the document at the location indicated by the specified url
        /// </summary>
        /// <param name="url"></param>
        public void Navigate(Uri url)
        {
            Navigate(url.AbsoluteUri);
        }

        /// <summary>
        /// Loads the document at the location indicated by the specified url
        /// </summary>
        /// <param name="url"></param>
        public void Navigate(string url)
        {
            if (_browser == null || _browser.GetMainFrame() == null)
                return;

            CefRequest request = CefRequest.Create();
            request.Url = url;
            _browser.GetMainFrame().LoadRequest(request);
        }

        /// <summary>
        /// Loads a local file as new document
        /// </summary>
        /// <param name="localfile"></param>
        public void NavigateToLocalFile(string localfile)
        {
            if (_browser == null)
                return;

            _browser.GetMainFrame().LoadUrl(localfile);
        }

        /// <summary>
        /// Reloads the document currently displayed
        /// </summary>
        public override void Refresh()
        {
            Refresh(false);     
        }

        /// <summary>
        /// Reloads the document currently displayed
        /// </summary>
        /// <param name="ignoreCache"></param>
        public void Refresh(bool ignoreCache)
        {
            if (_browser == null)
                return;

            if (ignoreCache)
                _browser.ReloadIgnoreCache();
            else
                _browser.Reload();
        }

        /// <summary>
        /// Cancels any pending navigation and stops any dynamic page elements
        /// </summary>
        public void Stop()
        {
            if (_browser == null)
                return;

            _browser.StopLoad();
        }

        /// <summary>
        /// Request JavaScript execution. Use the JavaScriptExecuted event to get any possible return values
        /// </summary>
        /// <param name="javascript"></param>
        public void ExecuteJavaScriptAsync(string javascript)
        {
            if (_browser == null)
                return;

            var message = CefProcessMessage.Create("EvalJSMessage");
            var arguments = message.Arguments;
            arguments.SetString(0, javascript);

            _browser.SendProcessMessage(CefProcessId.Renderer, message);
        }
        
        /// <summary>
        /// Executes a JavaScript statement immediately. No return value is given if you need the return value
        /// use the async method
        /// </summary>
        /// <param name="javascript"></param>
        public void ExecuteJavaScriptSynchronous(string javascript)
        {
            if (_browser == null)
                return;

            _browser.GetMainFrame().ExecuteJavaScript(javascript, string.Empty, 0);
        }

        /// <summary>
        /// Requests a screeshot of the current page. For full page screenshots the control must be used in Offscreen mode.
        /// </summary>
        /// <param name="desiredWidth"></param>
        /// <param name="desiredHeight"></param>
        public void TakePNGScreenshot(int desiredWidth, int desiredHeight)
        {
            if (this.IsOffScreen)
            {
                this.OffScreen_Width = desiredWidth > 0 ? desiredWidth : (this.OffScreen_Width + 1);    // plus one so we always have a real change to toggle the resize
                this.OffScreen_Height = desiredHeight > 8000 ? 8000 : desiredHeight;
                this.GettingScreenshot = true;

                _browser.GetHost().WasResized();
            }
            else
            {
                // if not in offscreen mode take visible screenshot
                Image ScreenshotImage = new Bitmap(this.Width, this.Height);

                TypeConverter tc = TypeDescriptor.GetConverter(typeof(Image));

                if (IsWin7() || IsVista() || IsXP())
                {
                    this.DrawToBitmap((Bitmap)ScreenshotImage, new Rectangle(0, 0, this.Width, this.Height));
                }
                else
                {
                    ScreenshotImage = (Image)tc.ConvertFrom(CaptureWindowPng(this.Handle));
                }

                using (MemoryStream ms = new MemoryStream())
                {
                    ScreenshotImage.Save(ms, ImageFormat.Png);
                    OnScreenshotReady(ms.ToArray());
                    ScreenshotImage.Dispose();
                }
            }

        }

        /// <summary>
        /// Opens the developer tools as described in the window info parameter
        /// </summary>
        /// <param name="windowInfo"></param>
        /// <param name="client"></param>
        public void ShowDevTools(CefWindowInfo windowInfo, CefClient client)
        {
            _browser.GetHost().ShowDevTools(windowInfo, client, new CefBrowserSettings(), new CefPoint(0, 0));
        }

        /// <summary>
        /// Closes the developer tools
        /// </summary>
        public void CloseDevTools()
        {
            _browser.GetHost().CloseDevTools();
        }

        /// <summary>
        /// Forces the control to recalculate the size
        /// </summary>
        public void ToggleResize()
        {
            if (_browser == null)
                return;

            _browser.GetHost().WasResized();
        }

        // Events

        /// <summary>
        /// Occurs if the browser can (or no longer can) go back
        /// </summary>
        public event EventHandler CanGoBackChanged;

        /// <summary>
        /// Occurs if the browser can (or no longer can) go forward
        /// </summary>
        public event EventHandler CanGoForwardChanged;
        private void UpdatedCommandStatus()
        {
            bool canGoBack = false;
            bool canGoForward = false;

            if (_browser != null)
            {
                canGoBack = _browser.CanGoBack;
                canGoForward = _browser.CanGoForward;
            }

            if (CanGoBack != canGoBack)
            {
                CanGoBack = canGoBack;
                if (CanGoBackChanged != null)
                    CanGoBackChanged(this, EventArgs.Empty);
            }

            if (CanGoForward != canGoForward)
            {
                CanGoForward = canGoForward;
                if (CanGoForwardChanged != null)
                    CanGoForwardChanged(this, EventArgs.Empty);
            }

        }

        /// <summary>
        /// Occurs if the browser has finished loading a document
        /// </summary>
        public event EventHandler<DocumentCompleteEventArgs> DocumentComplete;
        internal protected virtual void OnDocumentComplete(DocumentCompleteEventArgs e)
        {
            if (DocumentComplete != null)
                DocumentComplete(this, e);
        }

        /// <summary>
        /// Occurs if the title of the currently loaded document changes
        /// </summary>
        public event EventHandler<DocumentTitleChangedEventArgs> DocumentTitleChanged;
        internal protected virtual void OnDocumentTitleChanged(DocumentTitleChangedEventArgs e)
        {
            DocumentTitle = e.Title;

            var handler = DocumentTitleChanged;
            if (handler != null) handler(this, e);
        }

        /// <summary>
        /// Occurs before the browser starts a file download
        /// </summary>
        public event EventHandler FileDownload;
        internal void OnFileDownload(string suggestedName)
        {
            var handler = FileDownload;
            if (handler != null)
                handler(this, new FileDownloadEventArgs() { SuggestedFilename = suggestedName });
        }

        /// <summary>
        /// Occurs if the browser has finished navigating
        /// </summary>
        public event EventHandler<NavigatedEventArgs> Navigated;
        internal protected virtual void OnNavigated(NavigatedEventArgs e)
        {
            Url = new Uri(e.Address);

            var handler = Navigated;
            if (handler != null) handler(this, e);

            UpdatedCommandStatus();
        }

        /// <summary>
        /// Occurs if the browser navigates
        /// </summary>
        public event EventHandler<NavigatingEventArgs> Navigating;
        internal protected virtual void OnNavigating(NavigatingEventArgs e)
        {
            if (Navigating != null)
                Navigating(this, e);
        }

        /// <summary>
        /// Occurs before the browser opens a new window (popup)
        /// </summary>
        public event EventHandler<NewWindowEventArgs> NewWindow;
        internal protected virtual void OnNewWindow(NewWindowEventArgs e)
        {
            if (NewWindow != null)
                NewWindow(this, e);
            else
                e.Handled = false;
        }

        /// <summary>
        /// Occurs when the loading progress changes
        /// </summary>
        public event EventHandler<ProgressChangedEventArgs> ProgressChanged;
        internal protected virtual void OnProgressChanged(ProgressChangedEventArgs e)
        {
            this.IsBusy = e.IsLoading;

            if (ProgressChanged != null)
                ProgressChanged(this, e);

            UpdatedCommandStatus();
        }

        /// <summary>
        /// Occurs when the status text changes
        /// </summary>
        public event EventHandler<StatusTextEventArgs> StatusTextChanged;
        internal protected virtual void OnStatusTextChanged(StatusTextEventArgs e)
        {
            StatusText = e.Value;

            var handler = StatusTextChanged;
            if (handler != null) handler(this, e);

            UpdatedCommandStatus();
        }

        /// <summary>
        /// Occurs when any JavaScript has been executed through the ExecuteJavaScript method
        /// </summary>
        public event EventHandler<JSReturnEventArgs> JavaScriptReturn;
        internal void OnJavaScriptReturn(int type, bool returnBool, double returnDouble, string returnString, string javascript)
        {
            var handler = JavaScriptReturn;

            if (handler != null)
                handler(this, new JSReturnEventArgs() { JavaScript = javascript, Type = type, ReturnBool = returnBool, ReturnDouble = returnDouble, ReturnString = returnString });
        }

        /// <summary>
        /// Occurs as soon as a screenshot is ready.
        /// </summary>
        public event EventHandler<ScreenshotEventArgs> ScreenshotReady;
        internal void OnScreenshotReady(byte[] screenshot)
        {
            if (!GettingScreenshot)
                return;

            currentScreenshotRender += 1;

            if (currentScreenshotRender > MAX_SCREENSHOT_RENDERS)
            {
                return;
            }

            if (screenshotTimer != null)
                screenshotTimer.Stop();

            screenshotTimer = new Timer();
            screenshotTimer.Tick += ScreenshotTimer_Tick;
            screenshotTimer.Interval = 500;
            screenshotTimer.Tag = screenshot;
            screenshotTimer.Start();
        }

        public event EventHandler BrowserCreated;
        internal protected virtual void OnBrowserAfterCreated(CefBrowser browser)
        {
            _browser = browser;

            CefProcessMessage message = CefProcessMessage.Create("RegisterUA");
            message.Arguments.SetInt(0, browser.Identifier);
            message.Arguments.SetString(1, UserAgent);
            browser.SendProcessMessage(CefProcessId.Renderer, message);

            _browserWindowHandle = _browser.GetHost().GetWindowHandle();
            ResizeWindow(_browserWindowHandle, Width, Height);

            if (BrowserCreated != null)
                BrowserCreated(this, EventArgs.Empty);
        }

        public event EventHandler<PopupBrowserCreatedEventArgs> PopupBrowserCreated;
        internal protected virtual void OnPopupBrowserCreated(CefBrowser browser)
        {
            if (PopupBrowserCreated != null)
            {
                PopupBrowserCreated(this, new PopupBrowserCreatedEventArgs(browser));
            }
        }

        public event EventHandler<ConsoleMessageEventArgs> ConsoleMessage;
        internal protected virtual void OnConsoleMessage(ConsoleMessageEventArgs e)
        {
            if (ConsoleMessage != null)
                ConsoleMessage(this, e);
            else
                e.Handled = false;
        }

        public event EventHandler<TooltipEventArgs> Tooltip;
        internal protected virtual void OnTooltip(TooltipEventArgs e)
        {
            if (Tooltip != null)
                Tooltip(this, e);
            else
                e.Handled = false;
        }

        public event EventHandler BeforeClose;
        internal protected virtual void OnBeforeClose()
        {
            _browserWindowHandle = IntPtr.Zero;
            if (BeforeClose != null)
                BeforeClose(this, EventArgs.Empty);
        }

        public event EventHandler<LoadErrorEventArgs> LoadError;
        internal protected virtual void OnLoadError(LoadErrorEventArgs e)
        {
            if (LoadError != null)
                LoadError(this, e);
        }

        public event EventHandler<PluginCrashedEventArgs> PluginCrashed;
        internal protected virtual void OnPluginCrashed(PluginCrashedEventArgs e)
        {
            if (PluginCrashed != null)
                PluginCrashed(this, e);
        }

        public event EventHandler<RenderProcessTerminatedEventArgs> RenderProcessTerminated;
        internal protected virtual void OnRenderProcessTerminated(RenderProcessTerminatedEventArgs e)
        {
            if (RenderProcessTerminated != null)
                RenderProcessTerminated(this, e);
        }

        public event EventHandler DownloadProgress;
        internal void OnDownloadProgress(int percentComplete, bool complete)
        {
            var handler = DownloadProgress;
            if (handler != null)
                handler(this, new DownloadProgressEventArgs() { PercentComplete = percentComplete, IsComplete = complete });
        }

        public event EventHandler<CustomKeyEventArgs> KeyEvent;
        internal bool OnKeyEvent(CefKeyEvent keyEvent)
        {
            var handler = KeyEvent;

            CustomKeyEventArgs args = new CustomKeyEventArgs()
            {
                IsStrg = keyEvent.Modifiers == CefEventFlags.ControlDown,
                KeyCode = keyEvent.WindowsKeyCode
            };

            if (handler != null) handler(this, args);

            return true;
        }

        public event EventHandler<CreatePopupEventArgs> CreatePopup;
        internal CreatePopupEventArgs OnCreatePopup()
        {
            var handler = CreatePopup;

            if (handler != null)
            {
                CreatePopupEventArgs args = new CreatePopupEventArgs();
                handler(this, args);
                return args;
            }

            return null;
        }

        public new event EventHandler<ContextMenuEventArgs> ContextMenu;
        internal void OnContextMenu(string url)
        {
            var handler = ContextMenu;

            if (handler != null)
                handler(this, new ContextMenuEventArgs() { Url = url });
        }

        public event EventHandler<ActionPerformedEventArgs> ActionPerformed;
        internal void OnActionPerformed(string data)
        {
            var handler = ActionPerformed;

            if (handler != null)
                handler(this, new ActionPerformedEventArgs() { Data = data });
        }

        public event EventHandler<LogEventArgs> LogMessage;
        internal void OnLogMessage(string message, Exception ex)
        {
            if (LogMessage != null)
            {
                LogMessage(this, new LogEventArgs() { Exception = ex, Message = message });
            }
        }

        // Private

        internal void InvokeIfRequired(Action a)
		{
            try {
                if (InvokeRequired)
                    Invoke(a);
                else
                    a();
            }
            catch (ObjectDisposedException)
            {

            }
            catch (InvalidOperationException)
            {

            }
		}

        protected virtual CefWebClient CreateWebClient()
        {
            _client = new CefWebClient(this);
            return _client;
        }

        protected override void OnHandleCreated(EventArgs e)
        {
            base.OnHandleCreated(e);

            if (DesignMode)
            {
                if (!_handleCreated) Paint += PaintInDesignMode;
            }
            else
            {
                if (!WillBePopup)
                {
                    var windowInfo = CefWindowInfo.Create();

                    if (IsOffScreen)
                    {
                        windowInfo.SetAsWindowless(IntPtr.Zero, false);
                    }
                    else
                    {
                        windowInfo.SetAsChild(Handle, new CefRectangle { X = 0, Y = 0, Width = Width, Height = Height });
                    }

                    var client = CreateWebClient();

                    var settings = BrowserSettings;
                    if (settings == null) settings = new CefBrowserSettings { };

                    try
                    {
                        CefRequestContextSettings contextSettings = new CefRequestContextSettings();

                        contextSettings.IgnoreCertificateErrors = ignoreSSL;

                        CefRequestContextHandler contextHandler = new CefWebRequestContextHandler(BrowserIdentifier);
                        CefRequestContext context = CefRequestContext.CreateContext(contextSettings, contextHandler);
                        CefBrowserHost.CreateBrowser(windowInfo, client, settings, StartUrl, context);
                    }
                    catch (InvalidOperationException)
                    {
                        Application.Restart();
                    }
                }

                this.PreviewKeyDown += new PreviewKeyDownEventHandler(CefWebBrowser_PreviewKeyDown);
            }

            _handleCreated = true;            
        }

        void CefWebBrowser_PreviewKeyDown(object sender, PreviewKeyDownEventArgs arg)
        {
            if (HandledKeys.Contains(arg.KeyCode))
            {
                CefKeyEvent keyEvent = new CefKeyEvent()
                {
                    EventType = CefKeyEventType.RawKeyDown,
                    WindowsKeyCode = (int)arg.KeyCode,
                    NativeKeyCode = 0,
                    IsSystemKey = arg.Control
                };

                keyEvent.Modifiers = GetKeyboardModifiers(arg);

                _browser.GetHost().SendKeyEvent(keyEvent);
            }
        }

        private static CefEventFlags GetKeyboardModifiers(PreviewKeyDownEventArgs arg)
        {
            CefEventFlags modifiers = new CefEventFlags();

            if (arg.Modifiers == Keys.Alt)
                modifiers |= CefEventFlags.AltDown;

            if (arg.Modifiers == Keys.Control)
                modifiers |= CefEventFlags.ControlDown;

            if (arg.Modifiers == Keys.Shift)
                modifiers |= CefEventFlags.ShiftDown;

            return modifiers;
        }

        protected override void Dispose(bool disposing)
        {
            if (_browser != null && disposing) // TODO: ugly hack to avoid crashes when CefWebBrowser are Finalized and underlying objects already finalized
            {
                var host = _browser.GetHost();
                if (host != null)
                {
                    host.CloseBrowser();
                    host.Dispose();
                }
                _browser.Dispose();
                _browser = null;
                _browserWindowHandle = IntPtr.Zero;
            }

            base.Dispose(disposing);
        }

        public void AttachBrowser(CefBrowser browser)
        {
            _browser = browser;
            _browserWindowHandle = _browser.GetHost().GetWindowHandle();
            ResizeWindow(_browserWindowHandle, Width, Height);

            if (BrowserCreated != null)
                BrowserCreated(this, EventArgs.Empty);
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);

            if (_browserWindowHandle != IntPtr.Zero)
            {
                // Ignore size changes when form are minimized.
                var form = TopLevelControl as Form;
                if (form != null && form.WindowState == FormWindowState.Minimized)
                {
                    return;
                }

                ResizeWindow(_browserWindowHandle, Width, Height);
            }
        }

        private void PaintInDesignMode(object sender, PaintEventArgs e)
        {
            var width = this.Width;
            var height = this.Height;
            if (width > 1 && height > 1)
            {
                var brush = new SolidBrush(this.ForeColor);
                var pen = new Pen(this.ForeColor);
                pen.DashStyle = DashStyle.Dash;

                e.Graphics.DrawRectangle(pen, 0, 0, width - 1, height - 1);

                var fontHeight = (int)(this.Font.GetHeight(e.Graphics) * 1.25);

                var x = 3;
                var y = 3;

                e.Graphics.DrawString("CefWebBrowser", Font, brush, x, y + (0 * fontHeight));
                e.Graphics.DrawString(string.Format("StartUrl: {0}", StartUrl), Font, brush, x, y + (1 * fontHeight));

                brush.Dispose();
                pen.Dispose();
            }
        }

		public void InvalidateSize()
		{
			ResizeWindow(_browserWindowHandle, Width, Height);
		}

        private static void ResizeWindow(IntPtr handle, int width, int height)
        {
            if (handle != IntPtr.Zero)
            {
                NativeMethods.SetWindowPos(handle, IntPtr.Zero,
                    0, 0, width, height,
                    SetWindowPosFlags.NoMove | SetWindowPosFlags.NoZOrder
                    );
            }
        }

        internal CefBrowser Browser { get { return _browser; } }

        public bool GetAuthCredentials(out string username, out string password)
        {
            CredentialsDialog2 dialog = new CredentialsDialog2("Proxy Credentials needed");
            dialog.AlwaysDisplay = true;

            if (dialog.Show() == System.Windows.Forms.DialogResult.OK)
            {
                username = dialog.Name;
                password = dialog.Password;
                return true;
            }
            else
            {
                username = string.Empty;
                password = string.Empty;
                return false;
            }

        }

        internal bool OnBeforePluginLoad(CefWebPluginInfo into)
        {
            return DisablePlugins;
        }

        internal void OnScreenshotRequest(string callback)
        {
            if (LastScreenshot != null)
            {
                string result = Convert.ToBase64String(LastScreenshot);
                this.Browser.GetMainFrame().ExecuteJavaScript(callback + "('" + result + "');", string.Empty, 0);
            }
            else
            {
                this.Browser.GetMainFrame().ExecuteJavaScript(callback + "('');", string.Empty, 0);
            }
        }

        private void ScreenshotTimer_Tick(object sender, EventArgs e)
        {
            screenshotTimer.Stop();

            GettingScreenshot = false;
            currentScreenshotRender = 0;

            if (ScreenshotReady != null)
            {
                ScreenshotReady(this, new ScreenshotEventArgs() { Screenshot = (byte[])screenshotTimer.Tag });
            }

        }

        /// <summary>
        /// Tests if we run on windows xp
        /// </summary>
        /// <returns>True if XP, false otherwise</returns>
        private bool IsXP()
        {
            if (Environment.OSVersion.Version.Major == 5 && (Environment.OSVersion.Version.Minor == 1 || Environment.OSVersion.Version.Minor == 2))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Tests if we run on windows vista
        /// </summary>
        /// <returns>True if Vista, false otherwise</returns>
        private bool IsVista()
        {
            if (Environment.OSVersion.Version.Major == 6 && Environment.OSVersion.Version.Minor == 0)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Tests if we run on windows 7
        /// </summary>
        /// <returns>True if windows 7, false otherwise</returns>
        private bool IsWin7()
        {
            if (Environment.OSVersion.Version.Major == 6 && Environment.OSVersion.Version.Minor == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private byte[] CaptureWindowPng(IntPtr handle)
        {
            // get te hDC of the target window
            IntPtr hdcSrc = User32.GetWindowDC(handle);
            // get the size
            User32.RECT windowRect = new User32.RECT();
            User32.GetWindowRect(handle, ref windowRect);
            int width = windowRect.right - windowRect.left;
            int height = windowRect.bottom - windowRect.top;
            // create a device context we can copy to
            IntPtr hdcDest = Gdi32.CreateCompatibleDC(hdcSrc);
            // create a bitmap we can copy it to,
            // using GetDeviceCaps to get the width/height
            IntPtr hBitmap = Gdi32.CreateCompatibleBitmap(hdcSrc, width, height);
            // select the bitmap object
            IntPtr hOld = Gdi32.SelectObject(hdcDest, hBitmap);
            // bitblt over
            Gdi32.BitBlt(hdcDest, 0, 0, width, height, hdcSrc, 0, 0, Gdi32.SRCCOPY);
            // restore selection
            Gdi32.SelectObject(hdcDest, hOld);
            // clean up 
            Gdi32.DeleteDC(hdcDest);
            User32.ReleaseDC(handle, hdcSrc);
            byte[] ret = null;
            // get a .NET image object for it
            using (Bitmap img = Image.FromHbitmap(hBitmap))
            {
                // free up the Bitmap object
                Gdi32.DeleteObject(hBitmap);
                // Saving to stream
                using (var m = new MemoryStream())
                {
                    img.Save(m, ImageFormat.Png);
                    ret = m.ToArray();
                }
            }
            return ret;
        }
    }
}
