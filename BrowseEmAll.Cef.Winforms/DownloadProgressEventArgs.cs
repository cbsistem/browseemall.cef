﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrowseEmAll.Cef.Winforms
{
    public class DownloadProgressEventArgs:EventArgs
    {
        public int PercentComplete { get; set; }
        public bool IsComplete { get; set; }
    }
}
