﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrowseEmAll.Cef.Winforms
{
    public class CreatePopupEventArgs : EventArgs
    {
        public IntPtr Handle { get; set; }

        public CefWebBrowser Browser { get; set; }
    }
}
