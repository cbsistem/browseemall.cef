﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrowseEmAll.Cef.Winforms
{
    public class JSReturnEventArgs : EventArgs
    {
        // -1 nothing, 0 bool, 1 double, 2 string
        public int Type { get; set; }

        public bool ReturnBool { get; set; }

        public double ReturnDouble { get; set; }

        public string ReturnString { get; set; }

        public string JavaScript { get; set; }
    }
}
