﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrowseEmAll.Cef.Winforms
{
    public class CefWebV8Handler : CefV8Handler
    {
        private CefBrowser Browser;

        public CefWebV8Handler(CefBrowser browser)
        {
            this.Browser = browser;
        }

        protected override bool Execute(string name, CefV8Value obj, CefV8Value[] arguments, out CefV8Value returnValue, out string exception)
        {
            returnValue = null;
            exception = null;

            if (name.Equals("beacommunication"))
            {
                var returnmessage = CefProcessMessage.Create("ActionPerformedMessage");
                var messageArguments = returnmessage.Arguments;
                messageArguments.SetString(0,arguments[0].GetStringValue());

                Browser.SendProcessMessage(CefProcessId.Browser, returnmessage);

                return true;
            }
            else if (name.Equals("getscreenshot"))
            {
                var returnmessage = CefProcessMessage.Create("ScreenshotRequestMessage");
                var messageArguments = returnmessage.Arguments;
                messageArguments.SetString(0, arguments[0].GetStringValue());

                Browser.SendProcessMessage(CefProcessId.Browser, returnmessage);

                return true;
            }

            return false;
        }
    }
}
