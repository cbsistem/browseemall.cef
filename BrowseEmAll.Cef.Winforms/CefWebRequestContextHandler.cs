﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace BrowseEmAll.Cef.Winforms
{
    internal sealed class CefWebRequestContextHandler : CefRequestContextHandler
    {
        private readonly CefCookieManager manager;

        public CefWebRequestContextHandler(string browser)
        {
            manager = CefCookieManager.Create(string.Empty, false, null);   // Can I use null here?
        }

        protected override CefCookieManager GetCookieManager()
        {            
            return manager;
        }
    }
}
