﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrowseEmAll.Cef.Winforms
{
    public class LogEventArgs : EventArgs
    {
        public Exception Exception { get; set; }
        public string Message { get; set; }
    }
}
