﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace BrowseEmAll.Cef.Winforms
{
    internal class CefWebRenderHandler : CefRenderHandler
    {
        private readonly CefWebBrowser _core;

        public CefWebRenderHandler(CefWebBrowser core)
        {
            _core = core;
        }

        protected override bool GetScreenInfo(CefBrowser browser, CefScreenInfo screenInfo)
        {
            CefRectangle rect = new CefRectangle(0, 0, _core.OffScreen_Width, _core.OffScreen_Height);
            screenInfo.Rectangle = rect;
            screenInfo.AvailableRectangle = rect;
            return true;
        }

        protected override void OnCursorChange(CefBrowser browser, IntPtr cursorHandle, CefCursorType type, CefCursorInfo customCursorInfo)
        {
        }

        protected override void OnPaint(CefBrowser browser, CefPaintElementType type, CefRectangle[] dirtyRects, IntPtr buffer, int width, int height)
        {
            // We don't paint if there is not the correct size
            if ((width != _core.OffScreen_Width || height != _core.OffScreen_Height || !_core.GettingScreenshot))
                return;

            _core.OnLogMessage(string.Format("Type: {0} Buffer: {1:X8} Width: {2} Height: {3}", type, buffer, width, height), null);

            Rectangle rect = new Rectangle(0, 0, _core.OffScreen_Width, _core.OffScreen_Height);
            byte[] screenshot;

            try
            {
                screenshot = GenerateScreenshot(buffer, rect,_core.OffScreen_Width, _core.OffScreen_Height);
            }
            catch (ArgumentException)           
            {
                _core.OnLogMessage("ArgumentException", null);

                // too much memory taken
                rect.Height = height;
                screenshot = GenerateScreenshot(buffer, rect, _core.OffScreen_Width, height);
            }
            catch (AccessViolationException)
            {
                _core.OnLogMessage("ArgumentException", null);

                // too much memory taken
                rect.Height = height;
                screenshot = GenerateScreenshot(buffer, rect, _core.OffScreen_Width, height);
            }

            _core.OnScreenshotReady(screenshot);
        }

        private byte[] GenerateScreenshot(IntPtr buffer, Rectangle rect, int width, int height)
        {
            Bitmap screenshot = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            BitmapData ScreenShot = screenshot.LockBits(
               rect, System.Drawing.Imaging.ImageLockMode.WriteOnly,
               System.Drawing.Imaging.PixelFormat.Format32bppArgb
            );

            int totalSize = ScreenShot.Stride * ScreenShot.Height;

            byte[] TemporaryBuffer = null;

            if ((TemporaryBuffer == null) || (totalSize > TemporaryBuffer.Length))
                TemporaryBuffer = new byte[totalSize];

            IntPtr screenShotPtr = ScreenShot.Scan0;

            Marshal.Copy(buffer, TemporaryBuffer, 0, totalSize);
            Marshal.Copy(TemporaryBuffer, 0, screenShotPtr, totalSize);

            screenshot.UnlockBits(ScreenShot);

            using (MemoryStream ms = new MemoryStream())
            {
                screenshot.Save(ms, ImageFormat.Png);

                screenshot.Dispose();

                return ms.ToArray();
            }            
        }

        protected override bool GetViewRect(CefBrowser browser, ref CefRectangle rect)
        {
            rect = new CefRectangle(0, 0, _core.OffScreen_Width, _core.OffScreen_Height);
            return true;
        }

        protected override void OnPopupSize(CefBrowser browser, CefRectangle rect)
        {
            
        }

        protected override void OnScrollOffsetChanged(CefBrowser browser, double x, double y)
        {
            
        }
    }
}
