﻿using System;

namespace BrowseEmAll.Cef.Winforms
{
	public class ProgressChangedEventArgs : EventArgs
	{
		public ProgressChangedEventArgs(bool isLoading, bool canGoBack, bool canGoForward)
		{
			IsLoading = isLoading;
			CanGoBack = canGoBack;
			CanGoForward = canGoForward;
		}

		public bool IsLoading { get; private set; } 
		public bool CanGoBack { get; private set; }
		public bool CanGoForward { get; private set; } 
	}
}
