﻿namespace BrowseEmAll.Cef.Winforms
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal sealed class CefWebLifeSpanHandler : CefLifeSpanHandler
    {
        private readonly CefWebBrowser _core;

        public CefWebLifeSpanHandler(CefWebBrowser core)
        {
            _core = core;
        }

        protected override void OnAfterCreated(CefBrowser browser)
        {
            base.OnAfterCreated(browser);

            if (browser.GetHost().GetOpenerWindowHandle() != IntPtr.Zero)
            {
                _core.InvokeIfRequired(() => _core.OnPopupBrowserCreated(browser));
            }
            else
            {
                _core.InvokeIfRequired(() => _core.OnBrowserAfterCreated(browser));
            }        	
        }

        protected override bool DoClose(CefBrowser browser)
        {
            // TODO: ... dispose core
            return false;
        }

		protected override void OnBeforeClose(CefBrowser browser)
		{
            try {
                if (_core.InvokeRequired)
                    _core.BeginInvoke((Action)_core.OnBeforeClose);
                else
                    _core.OnBeforeClose();
            } catch (InvalidOperationException)
            {

            }
		}

        protected override bool OnBeforePopup(CefBrowser browser, CefFrame frame, string targetUrl, string targetFrameName, CefWindowOpenDisposition targetDisposition, bool userGesture, CefPopupFeatures popupFeatures, CefWindowInfo windowInfo, ref CefClient client, CefBrowserSettings settings, ref bool noJavascriptAccess)
        {
            if (popupFeatures.Width.HasValue || popupFeatures.Height.HasValue)
                return false;

            CreatePopupEventArgs args = _core.OnCreatePopup();

            if (args == null)
            {
                return false;
            }

            client = new CefWebClient(args.Browser);

            windowInfo.SetAsChild(args.Handle, new CefRectangle { X = 0, Y = 0, Width = 1024, Height = 768 });

            return false;
		}
    }
}
