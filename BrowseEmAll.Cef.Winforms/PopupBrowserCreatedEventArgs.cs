﻿using System;

namespace BrowseEmAll.Cef.Winforms
{
	public class PopupBrowserCreatedEventArgs : EventArgs
	{
		public PopupBrowserCreatedEventArgs(CefBrowser browser)
		{
            Browser = browser;
		}

		public CefBrowser Browser { get; private set; }
	}
}
