﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BrowseEmAll.Cef.Winforms
{
    public class CefWebRendererProcessHandler : CefRenderProcessHandler
    {
        private Dictionary<int, string> uastrings = new Dictionary<int, string>();

        public CefWebRendererProcessHandler()
        {
        }

        public void RegisterBrowser(int i, string ua)
        {
            uastrings.Add(i, ua);
        }

        protected override void OnContextCreated(CefBrowser browser, CefFrame frame, CefV8Context context)
        {
            CefV8Handler handler = new CefWebV8Handler(browser);

            CefV8Value window = context.GetGlobal();                     

            CefV8Value func = CefV8Value.CreateFunction("beacommunication", handler);
            window.SetValue("beacommunication", func, CefV8PropertyAttribute.None);

            if (uastrings.ContainsKey(browser.Identifier))
            {
                string jscode = "Object.defineProperty(window.navigator, 'userAgent', { get: function() { return '" + uastrings[browser.Identifier] + "';}});";
                frame.ExecuteJavaScript(jscode, frame.Url, 0);
            }
        }

        protected override void OnBrowserDestroyed(CefBrowser browser)
        {
        }

        protected override bool OnProcessMessageReceived(CefBrowser browser, CefProcessId sourceProcess, CefProcessMessage message)
        {
            if (message.Name.Equals("EvalJSMessage"))
            {
                string js = message.Arguments.GetString(0);

                if (!js.StartsWith("return"))
                    js = "return " + js;

                js = "function innerFunction() { " + js + "} innerFunction();";

                CefFrame frame = browser.GetMainFrame();

                CefV8Context context = frame.V8Context;

                CefV8Value returnValue;
                CefV8Exception exception;

                context.TryEval(js, out returnValue, out exception);

                var returnmessage = CefProcessMessage.Create("ResultJSMessage");
                var arguments = returnmessage.Arguments;
                if (returnValue != null)
                {
                    if (returnValue.IsBool)
                    {
                        arguments.SetInt(0, 0);
                        arguments.SetBool(1, returnValue.GetBoolValue());
                    }
                    else if (returnValue.IsDouble)
                    {
                        arguments.SetInt(0, 1);
                        arguments.SetDouble(1, returnValue.GetDoubleValue());
                    }
                    else if (returnValue.IsInt)
                    {
                        arguments.SetInt(0, 1);
                        arguments.SetInt(1, returnValue.GetIntValue());
                    }
                    else if (returnValue.IsString)
                    {
                        arguments.SetInt(0, 2);
                        arguments.SetString(1, returnValue.GetStringValue());
                    }
                    else
                    {
                        arguments.SetInt(0, -1);
                    }
                }

                arguments.SetString(2, js);

                browser.SendProcessMessage(CefProcessId.Browser, returnmessage);

                if (exception != null)
                    exception.Dispose();

                if (returnValue != null)
                    returnValue.Dispose();
            }
            else if (message.Name.Equals("RegisterUA"))
            {
                int identifier = message.Arguments.GetInt(0);
                string ua = message.Arguments.GetString(1);

                if (!uastrings.ContainsKey(identifier))
                    uastrings.Add(identifier, ua);
            }

            return false;
        }
    }
}
