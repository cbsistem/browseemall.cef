﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrowseEmAll.Cef.Winforms
{
    public class OpenPopupEventArgs:EventArgs
    {
        public CefBrowser newBrowser { get; set;}
    }
}
