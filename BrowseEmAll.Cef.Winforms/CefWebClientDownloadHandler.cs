﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace BrowseEmAll.Cef.Winforms
{
    internal sealed class CefWebClientDownloadHandler : CefDownloadHandler
    {
        private readonly CefWebBrowser _core;

        public CefWebClientDownloadHandler(CefWebBrowser core)
        {
            _core = core;
        }

        protected override void OnBeforeDownload(CefBrowser browser, CefDownloadItem downloadItem, string suggestedName, CefBeforeDownloadCallback callback)
        {
            _core.OnFileDownload(suggestedName);
            callback.Continue(NativeMethods.GetDownloadsPath() + Path.DirectorySeparatorChar + suggestedName, false);
        }

        protected override void OnDownloadUpdated(CefBrowser browser, CefDownloadItem downloadItem, CefDownloadItemCallback callback)
        {
            _core.OnDownloadProgress(downloadItem.PercentComplete, downloadItem.IsComplete);
        }

    }
}
