﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrowseEmAll.Cef.Winforms
{
    public class FileDownloadEventArgs:EventArgs
    {
        public string SuggestedFilename { get; set; }
    }
}
