﻿//
// This file manually written from cef/include/internal/cef_types.h.
//
namespace BrowseEmAll.Cef.Interop
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential, Pack = libcef.ALIGN)]
    internal unsafe struct cef_pdf_print_settings_t
    {
        public UIntPtr size;

        #region Alloc & Free
        private static int _sizeof;

        static cef_pdf_print_settings_t()
        {
            _sizeof = Marshal.SizeOf(typeof(cef_pdf_print_settings_t));
        }

        public static cef_pdf_print_settings_t* Alloc()
        {
            var ptr = (cef_pdf_print_settings_t*)Marshal.AllocHGlobal(_sizeof);
            *ptr = new cef_pdf_print_settings_t();
            ptr->size = (UIntPtr)_sizeof;
            return ptr;
        }

        public static void Free(cef_pdf_print_settings_t* ptr)
        {
            Marshal.FreeHGlobal((IntPtr)ptr);
        }
        #endregion
    }
}
