﻿//
// This file manually written from cef/include/internal/cef_types.h.
//
namespace BrowseEmAll.Cef.Interop
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential, Pack = libcef.ALIGN)]
    internal unsafe struct cef_uri_unescape_rule_t
    {
        public UIntPtr size;

        #region Alloc & Free
        private static int _sizeof;

        static cef_uri_unescape_rule_t()
        {
            _sizeof = Marshal.SizeOf(typeof(cef_uri_unescape_rule_t));
        }

        public static cef_uri_unescape_rule_t* Alloc()
        {
            var ptr = (cef_uri_unescape_rule_t*)Marshal.AllocHGlobal(_sizeof);
            *ptr = new cef_uri_unescape_rule_t();
            ptr->size = (UIntPtr)_sizeof;
            return ptr;
        }

        public static void Free(cef_uri_unescape_rule_t* ptr)
        {
            Marshal.FreeHGlobal((IntPtr)ptr);
        }
        #endregion
    }
}
