﻿//
// This file manually written from cef/include/internal/cef_types.h.
//
namespace BrowseEmAll.Cef.Interop
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential, Pack = libcef.ALIGN)]
    internal unsafe struct cef_draggable_region_t
    {
        public UIntPtr size;

        public cef_rect_t bounds;

        public int draggable;

        #region Alloc & Free
        private static int _sizeof;

        static cef_draggable_region_t()
        {
            _sizeof = Marshal.SizeOf(typeof(cef_draggable_region_t));
        }

        public static cef_draggable_region_t* Alloc()
        {
            var ptr = (cef_draggable_region_t*)Marshal.AllocHGlobal(_sizeof);
            *ptr = new cef_draggable_region_t();
            ptr->size = (UIntPtr)_sizeof;
            return ptr;
        }

        public static void Free(cef_draggable_region_t* ptr)
        {
            Marshal.FreeHGlobal((IntPtr)ptr);
        }
        #endregion
    }
}
