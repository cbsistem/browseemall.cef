﻿//
// This file manually written from cef/include/internal/cef_types.h.
// C API name: cef_menu_item_type_t.
//
namespace BrowseEmAll.Cef
{
    /// <summary>
    /// Supported menu item types.
    /// </summary>
    public enum CefMenuItemType
    {
        None,
        Command,
        Check,
        Radio,
        Separator,
        SubMenu,
    }
}
