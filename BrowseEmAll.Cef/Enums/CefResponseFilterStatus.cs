﻿//
// This file manually written from cef/include/internal/cef_types.h.
// C API name: cef_color_model_t.
//
namespace BrowseEmAll.Cef
{
    /// <summary>
    /// Response filter status variables
    /// </summary>
    public enum CefResponseFilterStatus
    {
        // Some or all of the pre-filter data was read successfully but more data is
        // needed in order to continue filtering (filtered output is pending).
        RESPONSE_FILTER_NEED_MORE_DATA,

        // Some or all of the pre-filter data was read successfully and all available
        // filtered output has been written.
        RESPONSE_FILTER_DONE,

        // An error occurred during filtering.
        RESPONSE_FILTER_ERROR
    }
}
