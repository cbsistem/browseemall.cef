﻿//
// This file manually written from cef/include/internal/cef_types.h.
// C API name: cef_color_model_t.
//
namespace BrowseEmAll.Cef
{
    /// <summary>
    /// Print job color mode values.
    /// </summary>
    public enum CefPluginPolicy
    {
        // Allow the content.
        PLUGIN_POLICY_ALLOW,

        // Allow important content and block unimportant content based on heuristics.
        // The user can manually load blocked content.
        PLUGIN_POLICY_DETECT_IMPORTANT,

        // Block the content. The user can manually load blocked content.
        PLUGIN_POLICY_BLOCK,

        // Disable the content. The user cannot load disabled content.
        PLUGIN_POLICY_DISABLE,
    }
}
